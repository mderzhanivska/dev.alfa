$(document).ready(function () {
    var $grid = $('.isotope');
    $grid.on('hidden.bs.collapse', function () {
        $grid.isotope('layout');
    })

    $('.card').on('show.bs.collapse', function () {
        var newheight = $(this).find('.collapse').height();
        $('#insurance_case_wrap .isotope').height(function (index, height) {
            return (height + newheight);
        });

    });
});
