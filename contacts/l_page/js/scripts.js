jQuery(document).ready(function () {
   
    jQuery('.map-params-list-clinics>div').click(function () {
        jQuery('.map-params-list-clinics').toggleClass('active');
    });
    
    jQuery('.map-params-list-line-clinics>div').click(function () {
        jQuery('.map-params-list-line-clinics').toggleClass('active');
        jQuery('.map-params-list-line-clinics').toggleClass('mouseIn');
    });
    
    jQuery('.map-params-list-line-clinics').mouseleave(function () {
        jQuery('.map-params-list-line-clinics').removeClass('mouseIn');
    });

    jQuery(document).click(function (event) {
        $target = $(event.target);
        if (!$target.closest('.map-dropdown').length 
            && $('.map-params-list-line-clinics').hasClass('active') 
            && !$('.map-params-list-line-clinics').hasClass('mouseIn')) {
            jQuery('.map-params-list-line-clinics').removeClass('active');
        }
    });
});
