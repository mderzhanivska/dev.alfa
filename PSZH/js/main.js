$(document).ready(function () {

    function triggerForm() {
        if ($('#child')[0].checked) {
            $('.adult-row').each(function () {
                $(this).addClass('hide');
            });
            $('.child-row').each(function () {
                $(this).removeClass('hide');
            });
        }
        if ($('#adult')[0].checked) {
            $('.adult-row').each(function () {
                $(this).removeClass('hide');
            });
            $('.child-row').each(function () {
                $(this).addClass('hide');
            });
        }
    }

    $('#trauma').on('click', function () {
        $('.promo-input').toggleClass("hide")
        $('.buy-btn').toggleClass("promomo")
    });

    $('#child').on('click', function () {
        triggerForm()
    });
    $('#adult').on('click', function () {
        triggerForm()
    });

    function hideModal() {
        $('.overlay').addClass("hide");
    }
    function showModal() {
        $('.overlay').removeClass("hide");
    }

    $('.showModal').each(function () {
        $(this).on("click", function (){
            showModal()
        })
    })
    $('.close-modal').on("click", function(){
        hideModal()
    })
    $('.overlay').on("click", function(){
        hideModal()
    })
    $('.modal-container').on("click", function(e){
        e.stopPropagation();
    })

    function acceptance() {
        if ($('#disability')[0].checked && $('#declaration')[0].checked) {
            return $('.buy-btn').removeClass('disabled')
        }
        if (!$('.buy-btn').hasClass('disabled')) {
            $('.buy-btn').addClass('disabled')
        }
    }

    $('#disability').on('click', function () {
        acceptance()
    });
    $('#declaration').on('click', function () {
        acceptance()
    });

    $('select').niceSelect();

    $('#start').datepicker({
        format: "dd-mm-yyyy",
        startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });

    jQuery(document).click(function (event) {
        $target = $(event.target);

        if (!$target.closest('.input-group-addon.date').length
            && $('.input-group-addon.date').hasClass('datepicker-opened')) {
            $('.input-group-addon.date').removeClass('datepicker-opened');
            $('.input-group-addon.date').closest('div').children('input').removeClass("dateisker-op-input");
        }
    });

    $('.input-group-addon.date').each(function () {
        console.log(this)
        $(this).on("click", function (e) {
            $(this).toggleClass('datepicker-opened');
            var input = $(this).closest('div').children('input');
            var inputWidth = $(input).closest('#date-wrap').width();
            $(input).toggleClass("dateisker-op-input");

            function open() {
                $("#" + input.attr('id')).datepicker('show');
            }
            function close() {
                $('.input-group-addon.date').removeClass('datepicker-opened');
                $('.input-group-addon.date').closest('div').children('input').removeClass("dateisker-op-input");
            }

            open()
            var datepicker = $('.datepicker')[0]; // set width to datepischer
            $(datepicker).width(inputWidth - 10);
            close()
            open()
            close()

            if ($(input).hasClass('dateisker-op-input')) {
                open()
            }
            $(datepicker).find('span').each(function () { // close on select
                $(this).on('click', close())
            })
        })
    });

    $('#first-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    $('#second-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    $('#third-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    $('#forth-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    $('#fifth-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });

    $('.input-group-addon.date').click(function () {
        //alert('clicked');
        $('#start').datepicker('show');
    });

    $('#first-datepicker').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });
    $('#second-datepicker').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });
    $('#third-datepicker').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });
    $('#end').change(function () {
        var end = $(this).val();
        if (end >= 1 && end <= 360) {
            $(this).removeClass('warning');
            $(this).addClass('success');

        } else {
            $(this).removeClass('success');
            $(this).addClass('warning');
        }
    });

    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        speed: 1000,
        initialSlide: 2,
        slideToClickedSlide: true,
        slidesPerView: 6,
        slidesPerGroup: 1,
        centeredSlides: true,
        loop: false,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {

            990: {
                slidesPerView: 1
            },
            1360: {
                slidesPerView: 3
            }
        },


        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    })

    var once = false;

    function countAnimation() {
        if (!once) {

            $('.count-number').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 5000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        }
        once = true;
    }

    function accordeon() {
        $(".card-header").each(function () {
            var cardHeader = this,
                open = $(cardHeader).find('.card-plus');
            $(open).addClass('close');
        });
    }

    $(".card-header").each(function () {
        var cardHeader = this,
            gates = $(cardHeader).next('.gates')[0],
            hiddenBlock = $(gates).find('.card-body')[0],
            open = $(cardHeader).find('.card-plus');

        open.on('click', function () {
            if ($(hiddenBlock).hasClass("opened-body")) {
                $(gates).css("height", 0 + "px");
                $(hiddenBlock).toggleClass('opened-body');
                $(open).toggleClass('close');
            } else {
                $(hiddenBlock).toggleClass('opened-body');
                $(gates).css("height", hiddenBlock.clientHeight + "px");
                $(open).toggleClass('close');
            }
        });
    });

    function animation($animation_elements, animationFunc) {
        var $window = $(window);

        function check_if_in_view() {
            var window_height = $window.height();
            var window_top_position = $window.scrollTop();
            var window_bottom_position = (window_top_position + window_height);

            $.each($animation_elements, function () {
                var $element = $(this);
                var element_height = $element.outerHeight();
                var element_top_position = $element.offset().top;
                var element_bottom_position = (element_top_position + element_height);

                //check to see if this current container is within viewport
                if ((element_bottom_position >= window_top_position) &&
                    (element_top_position <= window_bottom_position)) {
                    animationFunc();
                }
            });
        }

        $window.on('scroll resize', check_if_in_view);
    }

    animation($('.pszh-count-blck'), countAnimation);
    animation($('.dms-faq-list'), accordeon);


    $("#dms-scroll-blck a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1200, function () {
                window.location.hash = hash;
            });
        }
    });

    $('.extra-info').each(function () {
        $(this).on('mouseover', function () {
            $(this).addClass("opened");
        });
        $(this).mouseleave(function () {
            $(this).removeClass("opened");
        });
    });
});
