// list calendar
(function () {
    var MAX_COUNT_PEOPLE = 4;

    function showDatePicker(index) {
        var datePickerId = '#datepicker' + index;

        $(datePickerId).datepicker({
            format: "dd-mm-yyyy",
            startDate: "+1d",
            maxViewMode: 0,
            language: "uk",
            orientation: "bottom auto",
            autoclose: true,
            container: '#date-wrap-datepicker' + index,
            showOnFocus: "false"
        });

        $(datePickerId).datepicker('show');
    }

    $(document).ready(function () {
        var counterPerson = 0;

        function getDateInformationHTML(index, isAdd = false) {
            return `
            <div class="date-information active-datepicker" id="dateWrap${index}" data-index="${index}">
                <div class="date-field">
                    <label for="start">Дата народження</label>
                    <div class="input-group w-90 date date-picker" id="date-wrap-datepicker${index}">
                        <input type="text" class="form-control w-100" id="datepicker${index}" placeholder="" required>
                        <span class="input-group-addon datepicker${index}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28.004"
                                height="27.026" viewBox="0 0 28.004 27.026">
                                <path id="prefix__icon_calendar"
                                    d="M1399.955 1252.609a6.433 6.433 0 01-.2-1.6c0-.179.01-.356.024-.531a6.692 6.692 0 016.321-6.051c.145-.009.29-.015.437-.015a7.035 7.035 0 011.532.17 6.581 6.581 0 01-1.532 13.024 6.751 6.751 0 01-6.582-4.997zm2.1-1.6a4.487 4.487 0 104.485-4.362 4.425 4.425 0 00-4.488 4.367zm3.829-.638h4.156a4.1 4.1 0 01-4.156 4.043zm-18.816 2.234a1.726 1.726 0 01-1.75-1.7v-16.173a1.726 1.726 0 011.75-1.7h1.422v1.808a1.837 1.837 0 001.86 1.809h.65a1.837 1.837 0 001.859-1.809v-1.808h7.658v1.808a1.837 1.837 0 001.859 1.809h.656a1.837 1.837 0 001.86-1.809v-1.808h1.422a1.726 1.726 0 011.75 1.7v8.333a8.591 8.591 0 00-1.532-.14c-.147 0-.292 0-.437.012v-3.947h-18.816v10.215a1.3 1.3 0 001.313 1.277h9.646c-.012.175-.02.352-.02.531a7.842 7.842 0 00.164 1.6zm7.886-5.319a1.741 1.741 0 111.741 1.689 1.716 1.716 0 01-1.745-1.685zm-5.209 0a1.74 1.74 0 111.74 1.689 1.715 1.715 0 01-1.745-1.685zm10.419-5.11a1.741 1.741 0 111.741 1.69 1.716 1.716 0 01-1.745-1.686zm-5.21 0a1.741 1.741 0 111.741 1.69 1.716 1.716 0 01-1.745-1.686zm-5.209 0a1.74 1.74 0 111.74 1.69 1.715 1.715 0 01-1.745-1.686zm12.639-6.6a.755.755 0 01-.765-.744v-3.511a.756.756 0 01.765-.744h.656a.756.756 0 01.767.744v3.511a.756.756 0 01-.767.744zm-12.033 0a.756.756 0 01-.766-.744v-3.511a.756.756 0 01.766-.744h.649a.756.756 0 01.766.744v3.511a.756.756 0 01-.766.744z"
                                    data-name="icon calendar"
                                    transform="translate(-1385.314 -1230.584)" />
                            </svg>
                        </span>

                        ${isAdd ? `
                            <img class="add-birthday add-date-information" src="images/covid-page/more.svg" alt="age" />
                        `: `
                            <img class="add-birthday remove-date-information" src="images/covid-page/close.svg" alt="age" />
                        `}
                    </div>
                </div>
            </div>
        `;
        }

        function appendNewPicker(isAdd) {
            var children = $('.covid-calendar-list').children();
            if (children.length < MAX_COUNT_PEOPLE) {
                $('.covid-calendar-list').append(getDateInformationHTML(counterPerson, isAdd));
                counterPerson += 1;
            }
        }

        appendNewPicker(true);


        $('.covid-calendar-list').on('click', '.input-group-addon', function () {
            var index = $(this).closest('.date-information').data('index');
            showDatePicker(index);
        });

        $('select').niceSelect();

        $('.covid-calendar-list').on('click', '.remove-date-information', function () {
            var rootDateInformation = $(this).closest('.date-information');
            $(rootDateInformation).remove();
        });

        $('.covid-calendar-list').on('click', '.add-date-information', function () {
            appendNewPicker();
        });

        $('.covid-calendar-list').on('focus', '.form-control', function () {
            var index = $(this).closest('.date-information').data('index');
            showDatePicker(index);
        });
    });

    document.querySelector('.datepicker-trip-icon').addEventListener('click', function () {
        showDatePicker('-trip');
    })
    $('.to-save-trip').on('focus', '.form-control', function () {
        document.querySelector('#datepicker-trip').addEventListener('click', function () {
            showDatePicker('-trip');
        })
    });

})()