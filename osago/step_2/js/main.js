 $(document).ready(function () {

     $('select').niceSelect();
     $('#start').datepicker({
         format: "dd-mm-yyyy",
         startDate: "+1d",
         maxViewMode: 0,
         language: "uk",
         orientation: "bottom auto",
         autoclose: true,
         container: '#date-wrap',
         showOnFocus: "false"
     });
     $('.input-group-addon.date').click(function () {
         //alert('clicked');
         $('#start').datepicker('show');
     });

     $('#start').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
         "placeholder": "DD-MM-YYYY",
         onincomplete: function () {
             $(this).val('');
         }
     });

     $('input.index-field').inputmask({
         mask: "[9][9][9][9][9][9]",
         "placeholder": "",
         definitions: {
             '*': {
                 validator: "[0-9]",
             }
         }
     });

     $('input[type="tel"]').inputmask({
         mask: "+38(*{3})*{7}",
         "placeholder": "+38(___)_______",
         definitions: {
             '*': {
                 validator: "[0-9]",
             }
         }
     });

     $('input.email-field').inputmask({
         mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
         greedy: false,
         onBeforePaste: function (pastedValue, opts) {
             pastedValue = pastedValue.toLowerCase();
             return pastedValue.replace("mailto:", "");
         },
         definitions: {
             '*': {
                 validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                 casing: "lower"
             }
         }
     });
 });
