$(document).ready(function () {
    $('.dms-program-form input[name=tel]').inputmask({
        mask: "+38(*{3})*{7}",
        "placeholder": "+38(___)_______",
        definitions: {
            '*': {
                validator: "[0-9]",
            }
        }
    });

    $('.dms-program-form input[name=email]').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                casing: "lower"
            }
        }
    });
});
