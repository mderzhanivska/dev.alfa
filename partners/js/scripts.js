jQuery(document).ready(function () {

    jQuery('.map-params-title').click(function () {
        jQuery('.map-params-list').toggleClass('active');
    });

    jQuery(document).click(function (event) {
        $target = $(event.target);
        if (!$target.closest('.map-params').length &&
            $('.map-params-list').hasClass('active')) {
            jQuery('.map-params-list').removeClass('active');
        }
    });

    jQuery('.map-params-list-clinics>div').click(function () {
        jQuery('.map-params-list-clinics').toggleClass('active');
    });

    // // MENU UNDERLINE SLIDER
    function SliderMenu() {
        var li1o = document.getElementsByClassName("li1")[0];
        var li1 = window.getComputedStyle(li1o, null).getPropertyValue("width");

        var li2o = document.getElementsByClassName("li2")[0];
        var li2 = window.getComputedStyle(li2o, null).getPropertyValue("width");

        var li3o = document.getElementsByClassName("li3")[0];
        var li3 = window.getComputedStyle(li3o, null).getPropertyValue("width");

        var li4o = document.getElementsByClassName("li4")[0];
        var li4 = window.getComputedStyle(li4o, null).getPropertyValue("width");

        var clicked1 = 0;
        var clicked2 = "calc(" + li1 + ")";

        $(".underline").css({
            marginLeft: "0",
            width: "calc(" + li1 + ")"
        });

        $(".li1").click(function () {
            clicked1 = 0;
            clicked2 = "calc(" + li1 + ")";
            $(".underline").css({
                marginLeft: clicked1,
                width: clicked2
            });
        });
        //next
        $(".li2").click(function () {
            clicked1 = "calc(" + li1 + ")";
            clicked2 = "calc(" + li2 + ")";
            $(".underline").css({
                marginLeft: clicked1,
                width: clicked2
            });
        });
        //next
        $(".li3").click(function () {
            clicked1 = "calc(" + li2 + " + " + li1 + ")";
            clicked2 = "calc(" + li3 + ")";
            $(".underline").css({
                marginLeft: clicked1,
                width: clicked2
            });
        });
        //next
        $(".li4").click(function () {
            clicked1 = "calc(" + li1 + " + " + li2 + " + " + li3 + ")";
            clicked2 = "calc(" + li4 + ")";
            $(".underline").css({
                marginLeft: clicked1,
                width: clicked2
            });
        });
    }

    SliderMenu();

    $(window).resize(function () {
        SliderMenu();
    });
});
