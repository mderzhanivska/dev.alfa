$(document).ready(function () {
    $('select').niceSelect();

    $('input.name-input').inputmask({
        mask: "*{1,35} *{1,35}",
        "placeholder": "",
        definitions: {
            '*': {
                validator: "[A-Za-z]",
            }
        }
    });
    $('input.insurer-name').inputmask({
        mask: "*{1,35} *{1,35}",
        "placeholder": "",
        definitions: {
            '*': {
                validator: "[A-Za-z]",
            }
        }
    });

    $('input.insurer-tel').inputmask({
        mask: "+38(*{3})*{7}",
        "placeholder": "+38(___)_______",
        definitions: {
            '*': {
                validator: "[0-9]",
            }
        }
    });

    $('input.insurer-email').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                casing: "lower"
            }
        }
    });
    $('input.pas-1-name').change(function () {

        var pas_1_name = $(this).val();
        var insurance_1_name_elem = $('option#1').length;
        var insurance_1_name = $('option#1');
        if (pas_1_name) {
            if (insurance_1_name_elem) {
                $(insurance_1_name).val(pas_1_name);
                $(insurance_1_name).html(pas_1_name);
                $('select#insurance-name').niceSelect('update');

            } else {
                $('select#insurance-name').append("<option id='1' value='" + pas_1_name + "'>" + pas_1_name + "</option>");
                $('select#insurance-name').niceSelect('update');

            }
        }
    });

    $('input.pas-2-name').change(function () {

        var pas_2_name = $(this).val();
        var insurance_2_name_elem = $('option#2').length;
        var insurance_2_name = $('option#2');
        if (pas_2_name) {
            if (insurance_2_name_elem) {
                $(insurance_2_name).val(pas_2_name);
                $(insurance_2_name).html(pas_2_name);
                $('select#insurance-name').niceSelect('update');

            } else {
                $('select#insurance-name').append("<option id='2' value='" + pas_2_name + "'>" + pas_2_name + "</option>");
                $('select#insurance-name').niceSelect('update');

            }
        }
    });

    $('input.pas-3-name').change(function () {

        var pas_3_name = $(this).val();
        var insurance_3_name_elem = $('option#3').length;
        var insurance_3_name = $('option#3');
        if (pas_3_name) {
            if (insurance_3_name_elem) {
                $(insurance_3_name).val(pas_3_name);
                $(insurance_3_name).html(pas_3_name);
                $('select#insurance-name').niceSelect('update');

            } else {
                $('select#insurance-name').append("<option id='3' value='" + pas_3_name + "'>" + pas_3_name + "</option>");
                $('select#insurance-name').niceSelect('update');

            }
        }
    });
    $('input.pas-4-name').change(function () {

        var pas_4_name = $(this).val();
        var insurance_4_name_elem = $('option#4').length;
        var insurance_4_name = $('option#4');
        if (pas_4_name) {
            if (insurance_4_name_elem) {
                $(insurance_4_name).val(pas_4_name);
                $(insurance_4_name).html(pas_4_name);
                $('select#insurance-name').niceSelect('update');

            } else {
                $('select#insurance-name').append("<option id='4' value='" + pas_4_name + "'>" + pas_4_name + "</option>");
                $('select#insurance-name').niceSelect('update');

            }
        }
    });
    $('#insurance-name').change(function () {

        var pas_id = $("#insurance-name option:selected").attr("id");
        if (pas_id == '1') {
            $('.insurer-name').val($('.pas-1-name').val());
            $('#insurer-age-y').val($('#person-1-y option:selected').val());
            $('#insurer-age-y').niceSelect('update');
            $('#insurer-age-m').val($('#person-1-m option:selected').val());
            $('#insurer-age-m').niceSelect('update');
            $('#insurer-age-d').val($('#person-1-d option:selected').val());
            $('#insurer-age-d').niceSelect('update');
            $('.insurer-passport').val($('.pas-1-passport').val());
        } else if (pas_id == 2) {
            $('.insurer-name').val($('.pas-2-name').val());
            $('#insurer-age-y').val($('#person-2-y option:selected').val());
            $('#insurer-age-y').niceSelect('update');
            $('#insurer-age-m').val($('#person-2-m option:selected').val());
            $('#insurer-age-m').niceSelect('update');
            $('#insurer-age-d').val($('#person-2-d option:selected').val());
            $('#insurer-age-d').niceSelect('update');
            $('.insurer-passport').val($('.pas-2-passport').val());
        } else if (pas_id == 3) {
            $('.insurer-name').val($('.pas-3-name').val());
            $('#insurer-age-y').val($('#person-3-y option:selected').val());
            $('#insurer-age-y').niceSelect('update');
            $('#insurer-age-m').val($('#person-3-m option:selected').val());
            $('#insurer-age-m').niceSelect('update');
            $('#insurer-age-d').val($('#person-3-d option:selected').val());
            $('#insurer-age-d').niceSelect('update');
            $('.insurer-passport').val($('.pas-3-passport').val());
        } else if (pas_id == 4) {
            $('.insurer-name').val($('.pas-4-name').val());
            $('#insurer-age-y').val($('#person-4-y option:selected').val());
            $('#insurer-age-y').niceSelect('update');
            $('#insurer-age-m').val($('#person-4-m option:selected').val());
            $('#insurer-age-m').niceSelect('update');
            $('#insurer-age-d').val($('#person-4-d option:selected').val());
            $('#insurer-age-d').niceSelect('update');
            $('.insurer-passport').val($('.pas-4-passport').val());
        };
    });

});
