 $(document).ready(function () {
     $("input#travels").on('change', function () {
         var val = $(this).is(":checked");
         if (val == false) {
             $("#VzrCalculateForm_daysCount").val("");
             $("#travel-duration").removeClass('active');
             $(".input-group #end").show();
             $("label[for='end']").show();


         } else if (val == true) {
             $("#VzrCalculateForm_daysCount").val("");
             $("#travel-duration").addClass('active');
             $(".input-group #end").hide();
             $("label[for='end']").hide();

         }
     });

     $(".vzr-travel-duration").on('click', function () {
         var value = $(this).attr('id');
         $(".vzr-travel-duration").removeClass("active");
         $("#" + value).addClass("active");
         if (value == 'vzr-days30') {
             $("#VzrCalculateForm_daysCount").val(30);
         } else if (value == 'vzr-days60') {
             $("#VzrCalculateForm_daysCount").val(60);
         } else if (value == 'vzr-days90') {
             $("#VzrCalculateForm_daysCount").val(90);
         }
     });

     var i = 1;
     $("#vzr_person_add").on('click', function () {
         i++;
         $("#vzr_age_block_" + i).show();
         if (i >= 4) {
             $("#vzr_person_add").hide();
         }
     });

     $("#vzr-scroll-blck a").on('click', function (event) {
         if (this.hash !== "") {
             event.preventDefault();
             var hash = this.hash;
             $('html, body').animate({
                 scrollTop: $(hash).offset().top
             }, 1200, function () {
                 window.location.hash = hash;
             });
         }
     });

     $('#start').datepicker({
         format: "dd-mm-yyyy",
         startDate: "+1d",
         maxViewMode: 0,
         language: "uk",
         orientation: "bottom auto",
         autoclose: true,
         container: '#date-wrap',
         showOnFocus: "false"
     });
     $('.input-group-addon.date').click(function () {
         //alert('clicked');
         $('#start').datepicker('show');
     });

     $('#start').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
         "placeholder": "DD-MM-YYYY",
         onincomplete: function () {
             $(this).val('');
         }
     });
     $('#end').change(function () {
         var end = $(this).val();
         if (end >= 1 && end <= 360) {
             $(this).removeClass('warning');
             $(this).addClass('success');

         } else {
             $(this).removeClass('success');
             $(this).addClass('warning');
         }
     });
 });
