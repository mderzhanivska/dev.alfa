$(document).ready(function () {
    $('select').niceSelect();

    $("input#constructionElements").on('change', function () {
        var isChecked = $(this).is(":checked");

    });

    $("input#interior").on('change', function () {
        var isChecked = $(this).is(":checked");
        if (isChecked) {
            $('#interiorExtend').addClass('active-radio');
        } else {
            $('#interiorExtend').removeClass('active-radio');
        }
    });

    $("input#responsibility").on('change', function () {
        var isChecked = $(this).is(":checked");
        if (isChecked) {
            $('.insurance-input').addClass('active-input');
        } else {
            $('.insurance-input').removeClass('active-input');
        }
    });

    $("input#promo").on('change', function () {
        var isChecked = $(this).is(":checked");
        if (isChecked) {
            $('.promo-input').addClass('active-input');
        } else {
            $('.promo-input').removeClass('active-input');
        }
    });

    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        speed: 1000,
        initialSlide: 1,
        slideToClickedSlide: true,
        slidesPerView: 6,
        slidesPerGroup: 1,
        centeredSlides: true,
        loop: false,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {

            990: {
                slidesPerView: 1
            },
            1360: {
                slidesPerView: 3
            }
        },


        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    })



    $("#dms-scroll-blck a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1200, function () {
                window.location.hash = hash;
            });
        }
    });
});

// search open/close

$('#navbarSearch').on('click', function () {
    if ($('.search-icon').hasClass('active-icon')) {
        $('.search-icon').removeClass('active-icon');
        $('.close-icon').addClass('active-icon');
        $('.search-form').addClass('active-form');

    } else {
        $('.search-icon').addClass('active-icon');
        $('.close-icon').removeClass('active-icon');
        $('.search-form').removeClass('active-form');
    }

});
