$(document).ready(function () {
    $('select').niceSelect();

    $("#osago-scroll-blck a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1200, function () {
                window.location.hash = hash;
            });
        }
    });

    $(".osago_franchise_blck a").on("click", function (e) {
        e.preventDefault();
		var francisePrice = $(this).data('francise');
		$('#francise-total-price').html(francisePrice);
		if ($('.osago-result-price').hasClass('d-none')) {$('.osago-result-price').removeClass('d-none');}
		if ($('.btn-next-step').hasClass('disallow')) {$('.btn-next-step').removeClass('disallow');}
        $(".osago_franchise_blck a").removeClass("active");
        $(this).addClass("active");
    });
	
});
