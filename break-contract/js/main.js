$(document).ready(function () {
    $("input#travels").on('change', function () {
        var val = $(this).is(":checked");
        if (val == false) {
            $("#VzrCalculateForm_daysCount").val("");
            $("#travel-duration").removeClass('active');
            $(".input-group #end").show();
            $("label[for='end']").show();


        } else if (val == true) {
            $("#VzrCalculateForm_daysCount").val("");
            $("#travel-duration").addClass('active');
            $(".input-group #end").hide();
            $("label[for='end']").hide();

        }
    });

    $(".vzr-travel-duration").on('click', function () {
        var value = $(this).attr('id');
        $(".vzr-travel-duration").removeClass("active");
        $("#" + value).addClass("active");
        if (value == 'vzr-days30') {
            $("#VzrCalculateForm_daysCount").val(30);
        } else if (value == 'vzr-days60') {
            $("#VzrCalculateForm_daysCount").val(60);
        } else if (value == 'vzr-days90') {
            $("#VzrCalculateForm_daysCount").val(90);
        }
    });

    var i = 1;
    $("#vzr_person_add").on('click', function () {
        i++;
        $("#vzr_age_block_" + i).show();
        if (i >= 4) {
            $("#vzr_person_add").hide();
        }
    });

    $("#vzr-scroll-blck a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1200, function () {
                window.location.hash = hash;
            });
        }
    });

    $('#first-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    $('#second-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    $('#third-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    $('.input-group-addon.date').click(function () {
        //alert('clicked');
        var input = $(this).closest('div').children('input');
        var inputWidth = $('#date-wrap').width();
        $("#" + input.attr('id')).datepicker('show');
        $('.datepicker').width(inputWidth - 10);
        console.log(inputWidth);
    });

    $('#first-datepicker').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });
    $('#second-datepicker').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });
    $('#third-datepicker').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });
    $('#end').change(function () {
        var end = $(this).val();
        if (end >= 1 && end <= 360) {
            $(this).removeClass('warning');
            $(this).addClass('success');

        } else {
            $(this).removeClass('success');
            $(this).addClass('warning');
        }
    });

    $('.outline').on('click', function () {
        $('.outline').closest('.modal-wrap').addClass('hidden');
        $('body').removeClass('hidden-overflow');
    });

    $('#agree-with-conditions').on('change', function() {
        if (this.checked) {
            $('.btn-next-step').removeClass('disabled-btn');
        } else {
            $('.btn-next-step').addClass('disabled-btn');
        }
    }); 

    $('input[type="file"]').each(function () {
        var $file = $(this),
            $FileNameWrap = $file.siblings('#file-controls-wrap'),
            $FileName = $($FileNameWrap).find("#file-name"),
            $FileExtensionWrap = $($FileNameWrap).find("#file-extension"),
            $FileDelete = $($FileNameWrap).find("#delete-file"),
            tmppath = {};

        $FileDelete.on('click', function () {
            $FileNameWrap.addClass('hidden');
        });

        $FileName.on('click', function (event) {
            var userCenterScreen = window.innerHeight / 2;
            var top = 0;
            $('.modal-wrap > .outline').css("top", event.pageY + userCenterScreen - event.clientY + "px");
            $('.modal-wrap > .modal-body').css("top", event.pageY + userCenterScreen - event.clientY + "px");
            $('body').addClass('hidden-overflow');
            $('.modal-wrap').removeClass('hidden');
            $('.modal-wrap > .modal-body').empty();
            $('.modal-wrap > .modal-body').append('<img src="' + tmppath + '" />')
        });

        $file.on('change', function (event) {
            var fileName = $file.val().split('\\').pop(),
                fileExtension = "." + fileName.split('.').pop();

            tmppath = URL.createObjectURL(event.target.files[0]);

            $($FileName).empty();
            $($FileExtensionWrap).empty();
            $($FileNameWrap).removeClass('hidden');

            console.log(fileName);
            if (fileName) {
                $($FileName).append(fileName);
                $($FileExtensionWrap).append(fileExtension);
            }
        });
    });
});
