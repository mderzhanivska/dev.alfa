$(document).ready(function () {
    $('#subscription').on('click', function () {
        $('.subscription').addClass('active-tab-button');
        $('.contract').removeClass('active-tab-button');
        $('#subscriptionTab').addClass('show-tab');
        $('#contractTab').removeClass('show-tab');
    });

    $('#contract').on('click', function () {
        $('.contract').addClass('active-tab-button');
        $('.subscription').removeClass('active-tab-button');
        $('#contractTab').addClass('show-tab');
        $('#subscriptionTab').removeClass('show-tab');
    });

    $('#touristsQuantityContract').on('change', function () {
        console.log($("#touristsQuantityContract").val())
        console.log('clocl')
    });


    $('select').niceSelect();

    
    // $('#start,#tourist1,#tourist2,#tourist3').datepicker({
    //     format: "dd-mm-yyyy",
    //     startDate: "+1d",
    //     maxViewMode: 0,
    //     language: "uk",
    //     orientation: "bottom auto",
    //     autoclose: true,
    //     container: '#date-wrap',
    //     showOnFocus: "false"
    // })
    // $('#startContract,#tourist1Contract,#tourist2Contract,#tourist3Contract').datepicker({
    //     format: "dd-mm-yyyy",
    //     startDate: "+1d",
    //     maxViewMode: 0,
    //     language: "uk",
    //     orientation: "bottom auto",
    //     autoclose: true,
    //     container: '#date-wrap',
    //     showOnFocus: "false"
    // })
    $('#start').datepicker({
        format: "dd-mm-yyyy",
        startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap-start',
        showOnFocus: "false"
    });
    $('.input-group-addon.date').click(function () {
        //alert('clicked');
        $('#start').datepicker('show');
    });

    $('#tourist1').datepicker({
        format: "dd-mm-yyyy",
        startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap-tourist1',
        showOnFocus: "false"
    });
    $('.input-group-addon.tourist1').click(function () {
        //alert('clicked');
        $('#tourist1').datepicker('show');
    });

    $('#tourist2').datepicker({
        format: "dd-mm-yyyy",
        startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap-tourist2',
        showOnFocus: "false"
    });
    $('.input-group-addon.tourist2').click(function () {
        //alert('clicked');
        $('#tourist2').datepicker('show');
    });

    $('#tourist3').datepicker({
        format: "dd-mm-yyyy",
        startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap-tourist3',
        showOnFocus: "false"
    });
    $('.input-group-addon.tourist3').click(function () {
        //alert('clicked');
        $('#tourist3').datepicker('show');
    });

    $('#start').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });


    $('#tourist1').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });

    $('#tourist2').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });

    $('#tourist3').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });


    $('#term').on('change', function () {
        var optionVal = $("#term option:selected").val();

        if (
            optionVal === '7' ||
            optionVal === '14' ||
            optionVal === '21'
        ) {
            $('#bonus').addClass('show-bonus');
        } else {
            $('#bonus').removeClass('show-bonus');
        }
    });





    $('#startContract').datepicker({
        format: "dd-mm-yyyy",
        startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap-startContract',
        showOnFocus: "false"
    });
    $('.input-group-addon.startContract').click(function () {
        //alert('clicked');
        $('#startContract').datepicker('show');
    });

    $('#tourist1Contract').datepicker({
        format: "dd-mm-yyyy",
        startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap-tourist1Contract',
        showOnFocus: "false"
    });
    $('.input-group-addon.tourist1Contract').click(function () {
        //alert('clicked');
        console.log($('#tourist1Contract'))

        $('#tourist1Contract').datepicker('show');
    });

    $('#tourist2Contract').datepicker({
        format: "dd-mm-yyyy",
        startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap-tourist2Contract',
        showOnFocus: "false"
    });
    $('.input-group-addon.tourist2Contract').click(function () {
        //alert('clicked');
        $('#tourist2Contract').datepicker('show');
    });

    $('#tourist3Contract').datepicker({
        format: "dd-mm-yyyy",
        startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap-tourist3Contract',
        showOnFocus: "false"
    });
    $('.input-group-addon.tourist3Contract').click(function () {
        //alert('clicked');
        $('#tourist3Contract').datepicker('show');
    });

    $('#startContract').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });


    $('#tourist1Contract').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });

    $('#tourist2Contract').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });

    $('#tourist3Contract').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });


    $('#termContract').on('change', function () {
        var optionVal = $("#termContract option:selected").val();

        if (
            optionVal === '7' ||
            optionVal === '14' ||
            optionVal === '21'
        ) {
            $('#bonusContract').addClass('show-bonus');
        } else {
            $('#bonusContract').removeClass('show-bonus');
        }
    });

    $('#touristCount').on('change', function () {
        var optionVal = $("#touristCount option:selected").val();

        console.log(optionVal)

        if (optionVal === '1') {
            $('.age-field.tourist1').addClass('show-picker');
            $('.age-field.tourist2').removeClass('show-picker');
            $('.age-field.tourist3').removeClass('show-picker');
        }

        if (optionVal === '2') {
            $('.age-field.tourist2').addClass('show-picker');
            $('.age-field.tourist3').removeClass('show-picker');
        }

        if (optionVal === '3') {
            $('.age-field.tourist1').addClass('show-picker');
            $('.age-field.tourist2').addClass('show-picker');
            $('.age-field.tourist3').addClass('show-picker');
        }
    });

    $('#touristCountContract').on('change', function () {
        var optionVal = $("#touristCountContract option:selected").val();

        if (optionVal === '1') {
            $('.age-field.tourist1Contract').addClass('show-picker');
            $('.age-field.tourist2Contract').removeClass('show-picker');
            $('.age-field.tourist3Contract').removeClass('show-picker');
        }

        if (optionVal === '2') {
            $('.age-field.tourist2Contract').addClass('show-picker');
            $('.age-field.tourist3Contract').removeClass('show-picker');
        }

        if (optionVal === '3') {
            $('.age-field.tourist1Contract').addClass('show-picker');
            $('.age-field.tourist2Contract').addClass('show-picker');
            $('.age-field.tourist3Contract').addClass('show-picker');
        }
    });

});


 

