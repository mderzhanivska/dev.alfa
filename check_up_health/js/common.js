$(document).ready(function () {
    /** Converts numeric degrees to radians */
    if (typeof (Number.prototype.toRad) === 'undefined') {
        Number.prototype.toRad = function () {
            return this * Math.PI / 180;
        }
    }

    /** Converts numeric radians to degrees */
    if (typeof (Number.prototype.toDeg) === 'undefined') {
        Number.prototype.toDeg = function () {
            return this * 180 / Math.PI;
        }
    }

    var line = null;

    // Connect 2 divs with a line.
    function connect(div1, div2, color, thickness, z) {
        var p1 = findCenter(div1);
        var p2 = findCenter(div2);
        var length = lineDistance(p1, p2);
        var x = ((p1.x + p2.x) / 2) - (length / 2);
        var y = ((p1.y + p2.y) / 2) - (thickness / 2);
        var angle = Math.atan2((p1.y - p2.y), (p1.x - p2.x)).toDeg();

        if (!line) {
            line = $('<div />').addClass('line');
            $('body').append(line);
        }

        updateLine(line, length, angle, color, thickness, x, y, z);
    }

    // Get the top and left offsets for each div.
    function getOffset(el) {
        var _x = 0;
        var _y = 0;
        var _w = el.offsetWidth | 0;
        var _h = el.offsetHeight | 0;
        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            _x += el.offsetLeft - el.scrollLeft;
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }

        return {
            'top': _y,
            'left': _x,
            'width': _w,
            'height': _h
        };
    }

    // Find the center point for a div.
    function findCenter(div) {
        var off = getOffset(div);

        return {
            'x': off.left + off.width / 2,
            'y': off.top + off.height / 2
        };
    }

    // Line distance equation.
    function lineDistance(point1, point2) {
        var dx = point2.x - point1.x;
        dx *= dx;

        var dy = point2.y - point1.y;
        dy *= dy;

        return Math.sqrt(dx + dy);
    }

    // Generate a line div.
    function updateLine(line, length, angle, color, thickness, x, y, zIndex) {
        line.css('width', length);
        line.css('height', thickness);
        line.css('background-color', color);
        line.css('left', x + 'px');
        line.css('top', y + 'px');
        line.css('z-index', zIndex);

        rotateLine(line, angle)
    }

    function rotateLine(line, angle) {
        var rotation = 'rotate(' + angle + 'deg)';
        line.css('-moz-transform', rotation);
        line.css('-webkit-transform', rotation);
        line.css('-o-transform', rotation);
        line.css('-ms-transform', rotation);
        line.css('transform', rotation);
    }


    // %%% Swiper %%%
    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        speed: 1000,
        initialSlide: 1,
        slideToClickedSlide: true,
        slidesPerView: 6,
        slidesPerGroup: 1,
        centeredSlides: true,
        loop: false,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {

            990: {
                slidesPerView: 1
            },
            1360: {
                slidesPerView: 3
            }
        },


        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    })



    $("#dms-scroll-blck a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1200, function () {
                window.location.hash = hash;
            });
        }
    });

    document.querySelectorAll('[name=select1]').forEach(s => {
        s.addEventListener('change', function () {
            document.querySelectorAll('.check-health-siluet').forEach(d => d.classList.add('deactive'));
            document.getElementById(this.value).classList.remove('deactive');

            const findLine = document.querySelector('.line')
            findLine.classList.add('deactive')
        });
    });

    document.querySelectorAll('[name=select2]').forEach(s => {
        s.addEventListener('change', function (e) {
            document.querySelectorAll('.siluet-description').forEach(d => d.classList.add('deactive'));
            document.querySelectorAll('.siluet-dot').forEach(d => d.classList.remove('active'));

            document.getElementById(this.value).classList.remove('deactive');
            const toElem = document.querySelector(`#${this.value}`).querySelector('.line-connection-dot')

            if (this.parentNode.classList.contains(`${this.value}`) && toElem) {
                this.parentNode.classList.add('active')
                connect(this.parentNode, toElem, '#A1ABB3', 1, 3);
            }

            const findLine = document.querySelector('.line')
            findLine.classList.remove('deactive')
        });
    });
});
