$(document).ready(function () {
    $('select').niceSelect();

    $("#page-scroll-top a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1200, function () {
                window.location.hash = hash;
            });
        }
    });
    $('.property-form input[name=tel]').inputmask({
        mask: "+38(*{3})*{7}",
        "placeholder": "+38(___)_______",
        definitions: {
            '*': {
                validator: "[0-9]",
            }
        }
    });

    $('.property-form input[name=email]').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                casing: "lower"
            }
        }
    });
});
