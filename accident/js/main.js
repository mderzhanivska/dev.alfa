$(document).ready(function () {
    
    $('select').niceSelect();
     $('#start').datepicker({
         format: "dd-mm-yyyy",
         startDate: "+1d",
         maxViewMode: 0,
         language: "uk",
         orientation: "bottom auto",
         autoclose: true,
         container: '#date-wrap',
         showOnFocus: "false"
     });

     jQuery(document).click(function (event) {
         console.log("sadfsadf");
        $target = $(event.target);

        if (!$target.closest('.input-group-addon.date').length 
            && $('.input-group-addon.date').hasClass('datepicker-opened')) {
            $('.input-group-addon.date').removeClass('datepicker-opened');
            $('.input-group-addon.date').closest('div').children('input').removeClass("dateisker-op-input");
        }
    });

     $('.input-group-addon.date').click(function () {
        //alert('clicked');
        $(this).toggleClass('datepicker-opened');
        var input = $(this).closest('div').children('input');
        var inputWidth = $('#date-wrap').width();
        $(input).toggleClass("dateisker-op-input");

        if ($(input).hasClass('dateisker-op-input')) {
            $("#" + input.attr('id')).datepicker('show');
        }

        var datepicker = $('.datepicker');
        $(datepicker).width(inputWidth - 10);

        $(datepicker).find('span').each(function() {
           $(this).on('click', function() {
               $('.input-group-addon.date').removeClass('datepicker-opened');
               $('.input-group-addon.date').closest('div').children('input').removeClass("dateisker-op-input");
           })
        })

    });

     $('#first-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    $('#second-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    $('#third-datepicker').datepicker({
        format: "dd-mm-yyyy",
        // startDate: "+1d",
        maxViewMode: 0,
        language: "uk",
        orientation: "bottom auto",
        autoclose: true,
        container: '#date-wrap',
        showOnFocus: "false"
    });
    
     $('.input-group-addon.date').click(function () {
         //alert('clicked');
         $('#start').datepicker('show');
     });

     $('#first-datepicker').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });
    $('#second-datepicker').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });
    $('#third-datepicker').inputmask("[9][9]-[9][9]-[9][9][9][9]", {
        "placeholder": "DD-MM-YYYY",
        onincomplete: function () {
            $(this).val('');
        }
    });
     $('#end').change(function () {
         var end = $(this).val();
         if (end >= 1 && end <= 360) {
             $(this).removeClass('warning');
             $(this).addClass('success');

         } else {
             $(this).removeClass('success');
             $(this).addClass('warning');
         }
     });
     
     
     
    
    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        speed: 1000,
        initialSlide: 2,
        slideToClickedSlide: true,
        slidesPerView: 6,
        slidesPerGroup: 1,
        centeredSlides: true,
        loop: false,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {

            990: {
                slidesPerView: 1
            },
            1360: {
                slidesPerView: 3
            }
        },


        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    })


    $("#dms-scroll-blck a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1200, function () {
                window.location.hash = hash;
            });
        }
    });
});
