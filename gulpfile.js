var gulp = require("gulp"),
    sass = require("gulp-sass"),
    autoprefixer = require("autoprefixer"),
    postcss = require("gulp-postcss"),
    del = require('del'),
    browserSync = require("browser-sync").create();

var paths = {
    project: 'check_up_health',
    styles: {
        src: `${this.project}/scss/**/*.scss`,
        dist: `${this.project}/css`
    }
}

function style() {
    return gulp
        .src(paths.styles.src)
        .pipe(sass())
        .on("error", sass.logError)
        .pipe(postcss([autoprefixer(['last 15 versions'], { cascade: true })]))
        .pipe(gulp.dest(paths.styles.dist))
        .pipe(browserSync.stream());
}

function reload(done) {
    browserSync.reload();
    done();
}
function watch() {
    browserSync.init({
        server: {
            baseDir: `./`
        }
    });
    gulp.watch(paths.styles.src, style);
    gulp.watch(`${paths.project}/*.html`, reload);
}

exports.watch = watch
exports.style = style;

var build = gulp.parallel(style, watch);
gulp.task('default', build);