$(document).ready(function () {
    $('select').niceSelect();

    $('input.name-input').inputmask({
        mask: "*{1,35} *{1,35}",
        "placeholder": "",
        definitions: {
            '*': {
                validator: "[A-Za-z]",
            }
        }
    });
    $('input.insurer-name').inputmask({
        mask: "*{1,35} *{1,35}",
        "placeholder": "",
        definitions: {
            '*': {
                validator: "[A-Za-z]",
            }
        }
    });

    $('input.insurer-tel').inputmask({
        mask: "+38(*{3})*{7}",
        "placeholder": "+38(___)_______",
        definitions: {
            '*': {
                validator: "[0-9]",
            }
        }
    });

    $('input.insurer-email').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                casing: "lower"
            }
        }
    });
    $('input.pas-1-name').change(function () {

        var pas_1_name = $(this).val();
        var insurance_1_name_elem = $('option#1').length;
        var insurance_1_name = $('option#1');
        if (pas_1_name) {
            if (insurance_1_name_elem) {
                $(insurance_1_name).val(pas_1_name);
                $(insurance_1_name).html(pas_1_name);
                $('select#insurance-name').niceSelect('update');

            } else {
                $('select#insurance-name').append("<option id='1' value='" + pas_1_name + "'>" + pas_1_name + "</option>");
                $('select#insurance-name').niceSelect('update');

            }
        }
    });

    $('#insurance-name').change(function () {

        var pas_id = $("#insurance-name option:selected").attr("id");
        if (pas_id == '1') {
            $('.insurer-name').val($('.pas-1-name').val());
            $('#insurer-age-y').val($('#person-1-y option:selected').val());
            $('#insurer-age-y').niceSelect('update');
            $('#insurer-age-m').val($('#person-1-m option:selected').val());
            $('#insurer-age-m').niceSelect('update');
            $('#insurer-age-d').val($('#person-1-d option:selected').val());
            $('#insurer-age-d').niceSelect('update');
            $('.insurer-passport').val($('.pas-1-passport').val());
        };
    });

});
