// lines
const el1 = document.querySelector('#div1')
const el2 = document.querySelector('#div2')

/** Converts numeric degrees to radians */
if (typeof (Number.prototype.toRad) === 'undefined') {
    Number.prototype.toRad = function () {
        return this * Math.PI / 180;
    }
}

/** Converts numeric radians to degrees */
if (typeof (Number.prototype.toDeg) === 'undefined') {
    Number.prototype.toDeg = function () {
        return this * 180 / Math.PI;
    }
}

var line = 'undefined';

function handleConnectDivs() {
    // connect(div1, div2, '#007F00', 2, 3);
    connect(el1, el2, '#007F00', 2, 3);
}

handleConnectDivs();

// Connect 2 divs with a line.
function connect(div1, div2, color, thickness, z) {
    var p1 = findCenter(div1);
    var p2 = findCenter(div2);
    var length = lineDistance(p1, p2);
    var x = ((p1.x + p2.x) / 2) - (length / 2);
    var y = ((p1.y + p2.y) / 2) - (thickness / 2);
    var angle = Math.atan2((p1.y - p2.y), (p1.x - p2.x)).toDeg();

    if (line === 'undefined') {
        line = $('<div />').addClass('line');
        $('body').append(line);
    }

    updateLine(line, length, angle, color, thickness, x, y, z);
}

// Get the top and left offsets for each div.
function getOffset(el) {
    var _x = 0;
    var _y = 0;
    var _w = el.offsetWidth | 0;
    var _h = el.offsetHeight | 0;
    while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }

    return {
        'top': _y,
            'left': _x,
            'width': _w,
            'height': _h
    };
}

// Find the center point for a div.
function findCenter(div) {
    var off = getOffset(div);

    return {
        'x': off.left + off.width / 2,
            'y': off.top + off.height / 2
    };
}

// Line distance equation.
function lineDistance(point1, point2) {
    var dx = point2.x - point1.x;
    dx *= dx;

    var dy = point2.y - point1.y;
    dy *= dy;

    return Math.sqrt(dx + dy);
}

// Generate a line div.
function updateLine(line, length, angle, color, thickness, x, y, zIndex) {
    line.css('width', length);
    line.css('height', thickness);
    line.css('background-color', color);
    line.css('left', x + 'px');
    line.css('top', y + 'px');
    line.css('z-index', zIndex);

    rotateLine(line, angle)
}

function rotateLine(line, angle) {
    var rotation = 'rotate(' + angle + 'deg)';
    line.css('-moz-transform', rotation);
    line.css('-webkit-transform', rotation);
    line.css('-o-transform', rotation);
    line.css('-ms-transform', rotation);
    line.css('transform', rotation);
}